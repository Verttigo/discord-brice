let loop;

function startLoop() {
    loop = setInterval(change, 1000);
}

function change() {
    $.getJSON("Store.json", function (data) {
        for (let i = 0; i < 10; i++) {
            if (data.items[i].featured === 1) {
                document.getElementById("name" + (i + 1)).innerHTML = data.items[i].name + " [en vedette]";
            } else {
                document.getElementById("name" + (i + 1)).innerHTML = data.items[i].name;
            }
            document.getElementById("img" + (i + 1)).src = data.items[i].item.images.information + "?cache={lastupdate)";
            document.getElementById("type" + (i + 1)).innerHTML = "Type d'objet : " + french(data.items[i].item.type).bold();
            document.getElementById("rarity" + (i + 1)).innerHTML = "Rareté : " + french(data.items[i].item.rarity).bold();
        }
    });
}

function french(value) {
    if (value === "pickaxe") {
        return "Pioche";
    } else if (value === "emote") {
        return "Emoticône";
    } else if (value === "outfit") {
        return "Skin";
    } else if (value === "rare") {
        return "Rare";
    } else if (value === "epic") {
        return "Epic";
    } else if (value === "uncommon") {
        return "Ordinaire";
    } else if (value === "legendary") {
        return "Légendaire";
    } else if (value === "glider") {
        return "Planeur";
    } else if (value === "hard") {
        return "Difficile";
    } else if (value === "normal") {
        return "Normale";
    } else if (value === "pet") {
        return "Animal";
    } else if (value === "bundle") {
        return "Pack";
    }
}

function challenges() {
    let GoogleAPI;
    $.getJSON("Config.json", function (data) {
        GoogleAPI = data.GoogleAPI;
    });

    $.getJSON("Challenges.json", function (data) {
        document.getElementById("chal").innerHTML = "Nous sommes la semaine " + String(data.currentweek).bold() + " de la saison " + String(data.season).bold() + " de Fortnite";

        for (let i = 0; i <= 6; i++) {

            let url = "https://translation.googleapis.com/language/translate/v2";
            url += "?q=" + jsonPath(data, "$.challenges.week" + data.currentweek + ".[" + i + "].challenge");
            url += "&target=FR";
            //Mec STP utilise pas ma clef google translate, j'ai pas envie de me faire chier pour la cacher
            url += "&key=" + GoogleAPI;
            $.get(url, function (data, status) {
                document.getElementById("name" + (i + 1)).innerHTML = data.data.translations[0].translatedText;
            });
            let diffic = jsonPath(data, "$.challenges.week" + data.currentweek + ".[" + i + "].difficulty");
            let star = jsonPath(data, "$.challenges.week" + data.currentweek + ".[" + i + "].stars");

            document.getElementById("stars" + (i + 1)).innerHTML = "Étoiles : " + String(star).bold();
            document.getElementById("difficulty" + (i + 1)).innerHTML = "Difficulté : " + french(String(diffic)).bold();
        }

    });

}

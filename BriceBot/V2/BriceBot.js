const Discord = require("discord.js");
const timeDifference = require('date-timestamp-diff');
const fs = require('fs');
const request = require('request');

const http = require('http');
const https = require('https');
const express = require('express');

const app = express();


let config = require("./config.json");
let StoreData = require("./Web/Store.json");
let ChallengesData = require("./Web/Challenges.json");

let bot = new Discord.Client();
let guild;
let notifier;
let uptime;


bot.on("ready", async () => {
    console.log("Je suis prêt!");
    uptime = Date.now();
    bot.user.setPresence({
        game: {
            name: 'Verttigo♥',
            status: "idle",
            type: "WATCHING"
        }
    });
    guild = bot.guilds.find(guild => guild.name === "BriceBBrice - Discord");
    notifier = guild.roles.find(role => role.name === "» Notif Squad");
    if (await guild.roles.find(role => role.name == "» Notif Squad") === null) {
        guild.createRole({
            name: '» Notif Squad',
            color: 'GREEN',
            hoist: false,
            mentionable: false,
        })
            .then(console.log("Role pas créer donc => First Gen Role"))


    }
    startWeb();
    getStore();
    notify();

});

//Start Web Server
function startWeb() {
// Certificate

    const privateKey = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/privkey.pem', 'utf8');
    const certificate = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/cert.pem', 'utf8');
    const ca = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/chain.pem', 'utf8');

    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };

    app.use(express.static(__dirname + '/Web'));
    app.use(express.static(__dirname, {dotfiles: 'allow'}));

    app.use((req, res) => {
        res.send('Web Server started OK');
        res.sendFile(__dirname + '/Web/index.html');
    });


    http.createServer(function (req, res) {
        res.writeHead(301, {"Location": "https://brice.estle.best"});
        res.end();
    }).listen(80);

    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(443, () => {
        console.log('HTTPS Server running on port 443');
    });


}



bot.on('guildMemberAdd', member => {

    member.send("Bienvenue sur le discord de Brice " + member + "\n" + "\n" +
        "N'oublie pas d'aller lire les règles dans le channel #régles !\n" +
        "(T'inquiète pas, c'est pas très long et y'a plein d'infos utiles\n" + "\n"
        +
        "Squad Notification : \nPour ne pas rater les prochains live ou vidéo de Brice, tu peux activer les notification personnel en écrivant !notif dan n'importe quel channel du serveur!! "
        + "\nPour te remercier d'avoir activer les notifications, tu gagneras le grade Notif Squad qui te permetras d'avoir un grade en vert stylé!!\n").catch(console.error);
});

bot.on('error', error => {
    console.log("Erreur :( =>  " + JSON.stringify(error));
});


bot.on("message", message => {
    if (message.channel instanceof Discord.DMChannel) return;
    if (message.author.bot) return;
    let args = message.content.split(" ").slice(1);
    let command = message.content.split(" ")[0];
    command = command.slice(config.prefix.length);
    if (message.channel.name === "salon_image" || message.channel.name === "salon_vidéos") {
        if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name))) {
            if (message.attachments.size === 0) {
                message.author.send("Ce salon n'est pas fait pour parler").catch(console.error);
                message.delete();
            }
        }
    }
    if (message.channel.name === "général") {
        if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name))) {
            if (message.attachments.size != 0) {
                message.author.send("Ce salon n'est pas fait pour envoyer du contenu").catch(console.error);
                message.delete();
            }
        }
    }


    if (!message.content.startsWith(config.prefix)) return;
    if (command === "list?") {
        if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
            return message.author.send("TU N'AS PAS LA PERMISSION");
        let membersWithRole = message.guild.roles.get(notifier.id).members;
        let embed = new Discord.RichEmbed()
            .addField("Combien de personnes?", membersWithRole.size, true)
            .setColor(0x00AE86)
            .setTimestamp();
        message.author.send(embed).catch(console.error);
        message.delete();

    }


    if (command === "say") {
        if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
            return message.author.send("TU N'AS PAS LA PERMISSION");
        let membersWithRole = message.guild.roles.get(notifier.id).members;
        let sayMessage = args.join(" ");
        let embed = new Discord.RichEmbed()
            .addField("Message important!", sayMessage, true)
            .setColor(0x00AE86)
            .setTimestamp();
        membersWithRole.forEach(member => {
            member.send(embed).catch(console.error);
        });
        console.log(message.author.username + " a envoyé un message : " + sayMessage);
        message.delete();
    }
    if (command === "uptime") {
        if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
            return message.author.send("TU N'AS PAS LA PERMISSION");

        let mois = timeDifference.convertTimestampToAge(uptime, Date.now()).month + 0;
        let jours = timeDifference.convertTimestampToAge(uptime, Date.now()).day + 0;
        let minutes = timeDifference.convertTimestampToAge(uptime, Date.now()).minute;
        let seconds = timeDifference.convertTimestampToAge(uptime, Date.now()).second;

        let embed = new Discord.RichEmbed()
            .addField("Je suis en ligne depuis : ", mois + " moi(s), " + jours + " jour(s), " + minutes + " minute(s) et " + seconds + " seconde(s)!", true)
            .setColor(0x00AE86)
            .setTimestamp();
        message.author.send(embed).catch(console.error);
        message.delete();
    }

    if (command === "notif") {
        if (message.member.roles.has(notifier.id)) {
            message.member.removeRole(notifier.id);
            let embed = new Discord.RichEmbed()
                .setTitle("Notification désactivée")
                .setAuthor("BriceBot")
                .setColor(0x00AE86)
                .setTimestamp();
            message.author.send(embed).catch(console.error);
            console.log(message.author.username + " vient de désactiver les notifs")
        } else {
            message.member.addRole(notifier.id);
            let embed = new Discord.RichEmbed()
                .setTitle("Notification activée")
                .setAuthor("BriceBot")
                .setColor(0x00AE86)
                .setTimestamp();
            message.author.send(embed).catch(console.error);
            console.log(message.author.username + " vient d'activer  les notifs")
        }
        message.delete();

    }


});

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

async function getStore() {
    setInterval(async function () {
        request('https://fortnite-public-api.theapinetwork.com/prod09/store/get?language=en', {json: true}, async function (err, res, body) {
            if (err) {
                return console.log("Fortnite side");
            }
            if (isEmpty(body.items)) {
                return console.log("Body empty side");
            }

            if (isEmpty(StoreData)) {
                console.log("Premier chargement pas besoin de notif");
                fs.writeFileSync('./Web/Store.json', JSON.stringify(body));
                return;
            }
            if (StoreData.items[1].name !== body.items[1].name) {
                console.log("Ah changement de la boutique, notif");
                fs.writeFileSync('./Web/Store.json', JSON.stringify(body));
            }
        });
        request('https://fortnite-public-api.theapinetwork.com/prod09/challenges/get?season=current', {json: true}, async function (err, res, body) {
            if (err) {
                return console.log("Fortnite side");
            }
            if (isEmpty(body)) {
                return console.log("Body empty side");
            }

            if (isEmpty(ChallengesData)) {
                console.log("Premier chargement pas besoin de notif");
                fs.writeFileSync('./Web/Challenges.json', JSON.stringify(body));
                return;
            }
            if (ChallengesData.season !== body.season) {
                console.log("Ah changement de les chall, notif");
                fs.writeFileSync('./Web/Challenges.json', JSON.stringify(body));
            }
        });


    }, 1000 * 60);

}

async function notify() {
    let channel = await bot.channels.find(channels => channels.name === "annonces-lives");
    let actus = await bot.channels.find(channels => channels.name === "actus");
    let membersWithRole = await guild.roles.get(notifier.id).members;
    let live = false;
    let videoIds = null;

    //Check loop for live Only(1 minutes)
    setInterval(async function () {
        //LOOP For Youtube Live
        request('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC2mw5nP_FMpIN8NefBo4gAg&type=video&eventType=live&key=' + config.tokenYT, {json: true}, async function (err, res, body) {
            if (err) {
                return console.log("api side");
            }
            if (isEmpty(body.items.length)) {
                live = false;
            } else if (live == false) {
                live = true;
                console.log("Normalement Brice est en live Youtube");
                let data = await body.items[0];
                let embed = new Discord.RichEmbed()
                    .setTitle(JSON.stringify(data.snippet.title))
                    .setAuthor("⚠ Brice est en live sur Youtube ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                    .setColor(0x00AE86)
                    .setImage(data.snippet.thumbnails.high.url)
                    .setTimestamp()
                    .setURL("https://www.youtube.com/watch?v=" + data.id.videoId);

                membersWithRole.forEach(member => {
                    console.log("Message send à " + member.nickname);
                    member.send(embed).catch(console.error);
                });
                channel.send(embed);
            }
        });

        //LOOP For twitch Live
        request("https://api.twitch.tv/kraken/streams?client_id=" + config.TokenTW + "&channel=BriceBBrice", {json: true}, async function (err, res, body) {
            if (err) {
                return console.log("api side");
            }
            if (isEmpty(body.streams.length)) {
                live = false;
            } else if (live == false) {
                live = true;
                console.log("Normalement Brice est en live sur Twitch");
                let data = await body.streams[0];
                let embed = new Discord.RichEmbed()
                    .setTitle(JSON.stringify(data.channel.status))
                    .setAuthor("⚠ Brice est en live sur Twitch ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                    .setColor(0x00AE86)
                    .setImage("https://static-cdn.jtvnw.net/previews-ttv/live_user_BriceBBrice-390x256.jpg")
                    .setTimestamp()
                    .setURL("https://www.twitch.tv/bricebbrice");


                membersWithRole.forEach(member => {
                    console.log("Message send à " + member.nickname);
                    // member.send(embed).catch(console.error);
                });
                //channel.send(embed);
            }

        });

    }, 1000 * 60);

//Check loop for Video Only (5 minutes)
    setInterval(async function () {
        request('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC2mw5nP_FMpIN8NefBo4gAg&maxResults=1&order=date&type=video&key=' + config.tokenYT, {json: true}, async function (err, res, body) {
            if (err) {
                return console.log("api side");
            }
            if (videoIds === null) {
                videoIds = await body.items[0].id.videoId;
                console.log("Pas d'id donc je prend " + body.items[0].id.videoId);
                return;
            }

            if (!(videoIds === body.items[0].id.videoId)) {
                //nouvelle video
                console.log("Sortie vidéo");
                let data = await body.items[0];
                let embed = new Discord.RichEmbed()
                    .setTitle(JSON.stringify(data.snippet.title))
                    .setAuthor("⚠ Brice a sorti une nouvelle vidéo ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                    .setColor(0x00AE86)
                    .setDescription(data.snippet.description)
                    .setImage(data.snippet.thumbnails.high.url)
                    .setTimestamp()
                    .setURL("https://www.youtube.com/watch?v=" + data.id.videoId);

                membersWithRole.forEach(member => {
                    member.send(embed).catch(console.error);
                });

                actus.send(embed);
                videoIds = body.items[0].id.videoId;
            }

        });

    }, 1000 * 60 * 5);
}


bot.login(config.token);
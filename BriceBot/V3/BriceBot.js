const Discord = require("discord.js");
const timeDifference = require('date-timestamp-diff');
const fs = require('fs');
const Canvas = require('canvas');
const request = require('request');

const privateKey = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/brice.estle.best/chain.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};

const http = require('http');
const https = require('https');
const express = require('express');
const app = express();
const httpsServer = https.createServer(credentials, app);

const io = require('socket.io')(httpsServer);
const jp = require('jsonpath');

let config = require("./config.json");

let bot = new Discord.Client();

// je ne sais pas pourquoi je ne peux pas les stacker
let name = [];
let img = [];
let type = [];
let rarity = [];
let stars = [];
let difficulty = [];
let objective = [];
let week, season, uptime, date, channel, actus, guild, notifier, membersWithRole;
let live = false;
let videoIds = null;

//canvas Coord
const coordinates = [
    [0, 0],
    [400, 0],
    [800, 0],
    [1200, 0],
    [1600, 0],
    [0, 400],
    [400, 400],
    [800, 400],
    [1200, 400],
    [1600, 400],
    [0, 800],
    [400, 800],
    [800, 1200],
    [1200, 1600],
    [1600, 0],
];


bot.on("ready", async () => {
    console.log("Je suis prêt!");
    uptime = Date.now();
    bot.user.setPresence({
        game: {
            name: 'Verttigo♥',
            status: "idle",
            type: "WATCHING"
        }
    });
    await initVaraible();
    await createRole();
    startWeb();
    initLoop();

});

async function createRole() {
    if (await guild.roles.find(role => role.name == "» Notif Squad") === null) {
        guild.createRole({
            name: '» Notif Squad',
            color: 'GREEN',
            hoist: false,
            mentionable: false,
        })
            .then(console.log("Role pas créer donc => First Gen Role"))
    }
}

async function initVaraible() {
    channel = bot.channels.find(channels => channels.name === "annonces-lives");
    actus = bot.channels.find(channels => channels.name === "actus");
    guild = bot.guilds.find(guild => guild.name === "BriceBBrice - Discord");
    notifier = guild.roles.find(role => role.name === "» Notif Squad");
    membersWithRole = guild.roles.get(notifier.id).members;
}


//Start Web Server
function startWeb() {
// Certificate


    app.use(express.static(__dirname + '/Web'));
    app.use(express.static(__dirname, {dotfiles: 'allow'}));

    app.use((req, res) => {
        res.send('Web Server started OK');
        res.sendFile(__dirname + '/Web/index.html');
    });


    http.createServer(function (req, res) {
        res.writeHead(301, {"Location": "https://brice.estle.best"});
        res.end();
    }).listen(80);

    httpsServer.listen(443, () => {
        console.log('HTTPS Server running on port 443');
        console.log("Loading Store and challenges");
    });


}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

io.on('connection', function (socket) {
    socket.emit('store', name, img, type, rarity);
    socket.emit('challenges', week, objective, stars, difficulty, season);


});


function initLoop() {
    getStore();
    getChall();
    newPost();
    newTwitchLive();
    newYTlive();

    setInterval(function () {
        newPost();
        newTwitchLive();
        newYTlive();
    }, 1000 * 60 * 5);
    setInterval(function () {
        getStore();
        getChall();
    }, 1000 * 60 * 60);
}


bot.on('guildMemberAdd', member => {

    member.send("Bienvenue sur le discord de Brice " + member + "\n" + "\n" +
        "N'oublie pas d'aller lire les règles dans le channel #régles !\n" +
        "(T'inquiète pas, c'est pas très long et y'a plein d'infos utiles\n" + "\n"
        +
        "Squad Notification : \nPour ne pas rater les prochains live ou vidéo de Brice, tu peux activer les notification personnel en écrivant !notif dan n'importe quel channel du serveur!! "
        + "\nPour te remercier d'avoir activer les notifications, tu gagneras le grade Notif Squad qui te permetras d'avoir un grade en vert stylé!!\n").catch(console.error);
});

bot.on('error', error => {
    console.log("Erreur :( =>  " + JSON.stringify(error));
});


bot.on("message", async message => {
        if (message.channel instanceof Discord.DMChannel) return;
        if (message.author.bot) return;
        let args = message.content.split(" ").slice(1);
        let command = message.content.split(" ")[0];
        command = command.slice(config.prefix.length);
        if (message.channel.name === "salon_image" || message.channel.name === "salon_vidéos") {
            if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name))) {
                if (message.attachments.size === 0) {
                    message.author.send("Ce salon n'est pas fait pour parler").catch(console.error);
                    message.delete();
                }
            }
        }
        if (command === "boutique") {
            message.delete();
            const canvas = Canvas.createCanvas(2000, 800);
            const ctx = canvas.getContext('2d');
            await request('https://fortnite-public-api.theapinetwork.com/prod09/store/get?language=en', {json: true}, async function (err, res, body) {
                if (err) {
                    return console.log("Fortnite side");
                }
                const background = await Canvas.loadImage('https://cdn.glitch.com/6f19ff8d-183d-4fc7-826a-db14e0b212fc%2Fimage.png?1551782746329');
                ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
                for (let i = 0; i < body.items.length; i++) {
                    let loadImage = await Canvas.loadImage(body.items[i].item.images.information + "?cache={lastupdate)");
                    ctx.drawImage(loadImage, coordinates[i][0], coordinates[i][1], 400, 400);
                }

                const attachment = new Discord.Attachment(await canvas.toBuffer(), 'fortnite-today.png');

                const embed = new Discord.RichEmbed()
                    .attachFiles([attachment])
                    .setImage('attachment://fortnite-today.png');
                message.author.send("** Aujourd'hui dans la boutique Fortnite : **", {embed: embed})

            });
        }
        if (command === "coming") {
            message.delete();
            const canvas = Canvas.createCanvas(2000, 800);
            const ctx = canvas.getContext('2d');
            await request('https://fortnite-public-api.theapinetwork.com/prod09/upcoming/get', {json: true}, async function (err, res, body) {
                if (err) {
                    return console.log("Fortnite side");
                }
                const background = await Canvas.loadImage('https://cdn.glitch.com/6f19ff8d-183d-4fc7-826a-db14e0b212fc%2Fimage.png?1551782746329');
                ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
                for (let i = 0; i < body.items.length; i++) {
                    let loadImage = await Canvas.loadImage(body.items[i].item.images.information + "?cache={lastupdate)");
                    ctx.drawImage(loadImage, coordinates[i][0], coordinates[i][1], 400, 400);
                }

                const attachment = new Discord.Attachment(await canvas.toBuffer(), 'fortnite-coming.png');

                const embed = new Discord.RichEmbed()
                    .attachFiles([attachment])
                    .setImage('attachment://fortnite-coming.png');
                message.author.send("** Bientôt dans la boutique Fortnite : **", {embed: embed})

            });
        }


        if (message.channel.name === "général") {
            if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name))) {
                if (message.attachments.size != 0) {
                    message.author.send("Ce salon n'est pas fait pour envoyer du contenu").catch(console.error);
                    message.delete();
                }
            }
        }


        if (!message.content.startsWith(config.prefix)) return;
        if (command === "list?") {
            if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
                return message.author.send("TU N'AS PAS LA PERMISSION");
            let membersWithRole = message.guild.roles.get(notifier.id).members;
            let embed = new Discord.RichEmbed()
                .addField("Combien de personnes?", membersWithRole.size, true)
                .setColor(0x00AE86)
                .setTimestamp();
            message.author.send(embed).catch(console.error);
            message.delete();

        }


        if (command === "say") {
            if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
                return message.author.send("TU N'AS PAS LA PERMISSION");
            let membersWithRole = message.guild.roles.get(notifier.id).members;
            let sayMessage = args.join(" ");
            let embed = new Discord.RichEmbed()
                .addField("Message important!", sayMessage, true)
                .setColor(0x00AE86)
                .setTimestamp();
            membersWithRole.forEach(member => {
                member.send(embed).catch(console.error);
            });
            console.log(message.author.username + " a envoyé un message : " + sayMessage);
            message.delete();
        }
        if (command === "uptime") {
            if (!message.member.roles.some(r => ["» Administrateur", "» Modérateur", "» Administrateur Suprême", "SUPRÊME"].includes(r.name)))
                return message.author.send("TU N'AS PAS LA PERMISSION");

            let mois = timeDifference.convertTimestampToAge(uptime, Date.now()).month + 0;
            let jours = timeDifference.convertTimestampToAge(uptime, Date.now()).day + 0;
            let minutes = timeDifference.convertTimestampToAge(uptime, Date.now()).minute;
            let seconds = timeDifference.convertTimestampToAge(uptime, Date.now()).second;

            let embed = new Discord.RichEmbed()
                .addField("Je suis en ligne depuis : ", mois + " moi(s), " + jours + " jour(s), " + minutes + " minute(s) et " + seconds + " seconde(s)!", true)
                .setColor(0x00AE86)
                .setTimestamp();
            message.author.send(embed).catch(console.error);
            message.delete();
        }

        if (command === "notif") {
            if (message.member.roles.has(notifier.id)) {
                message.member.removeRole(notifier.id);
                let embed = new Discord.RichEmbed()
                    .setTitle("Notification désactivée")
                    .setAuthor("BriceBot")
                    .setColor(0x00AE86)
                    .setTimestamp();
                message.author.send(embed).catch(console.error);
                console.log(message.author.username + " vient de désactiver les notifs")
            } else {
                message.member.addRole(notifier.id);
                let embed = new Discord.RichEmbed()
                    .setTitle("Notification activée")
                    .setAuthor("BriceBot")
                    .setColor(0x00AE86)
                    .setTimestamp();
                message.author.send(embed).catch(console.error);
                console.log(message.author.username + " vient d'activer  les notifs")
            }
            message.delete();

        }


    }
);


function newYTlive() {

    //LOOP For Youtube Live
    request('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC2mw5nP_FMpIN8NefBo4gAg&type=video&eventType=live&key=' + config.tokenYT, {json: true}, async function (err, res, body) {
        if (err) {
            return console.log("api side");
        }
        if (body.items.length === 0) {
            live = false;
        } else if (live == false) {
            live = true;
            console.log("Normalement Brice est en live Youtube");
            let data = await body.items[0];
            let embed = new Discord.RichEmbed()
                .setTitle(JSON.stringify(data.snippet.title))
                .setAuthor("⚠ Brice est en live sur Youtube ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                .setColor(0x00AE86)
                .setImage(data.snippet.thumbnails.high.url)
                .setTimestamp()
                .setURL("https://www.youtube.com/watch?v=" + data.id.videoId);

            membersWithRole.forEach(member => {
                console.log("Message send à " + member.nickname);
                member.send(embed).catch(console.error);
            });
            channel.send(embed);
        }
    });
}

function newTwitchLive() {
    request("https://api.twitch.tv/kraken/streams?client_id=" + config.TokenTW + "&channel=BriceBBrice", {json: true}, async function (err, res, body) {
        if (err) {
            return console.log("api side");
        }
        if (body.streams.length === 0) {
            live = false;
        } else if (live == false) {
            live = true;
            console.log("Normalement Brice est en live sur Twitch");
            let data = await body.streams[0];
            let embed = new Discord.RichEmbed()
                .setTitle(JSON.stringify(data.channel.status))
                .setAuthor("⚠ Brice est en live sur Twitch ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                .setColor(0x00AE86)
                .setImage("https://static-cdn.jtvnw.net/previews-ttv/live_user_BriceBBrice-390x256.jpg")
                .setTimestamp()
                .setURL("https://www.twitch.tv/bricebbrice");


            membersWithRole.forEach(member => {
                console.log("Message send à " + member.nickname);
                // member.send(embed).catch(console.error);
            });
            //channel.send(embed);
        }

    });

}

function newPost() {
    request('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC2mw5nP_FMpIN8NefBo4gAg&maxResults=1&order=date&type=video&key=' + config.tokenYT, {json: true}, async function (err, res, body) {
        if (err) {
            return console.log("api side");
        }
        if (videoIds === null) {
            videoIds = await body.items[0].id.videoId;
            console.log("Pas d'id donc je prend " + body.items[0].id.videoId);
            return;
        }

        if (!(videoIds === body.items[0].id.videoId)) {
            //nouvelle video
            console.log("Sortie vidéo");
            let data = await body.items[0];
            let embed = new Discord.RichEmbed()
                .setTitle(JSON.stringify(data.snippet.title))
                .setAuthor("⚠ Brice a sorti une nouvelle vidéo ! ⚠", "https://i.imgur.com/T3yTYfY.png")
                .setColor(0x00AE86)
                .setDescription(data.snippet.description)
                .setImage(data.snippet.thumbnails.high.url)
                .setTimestamp()
                .setURL("https://www.youtube.com/watch?v=" + data.id.videoId);

            membersWithRole.forEach(member => {
                member.send(embed).catch(console.error);
            });

            actus.send(embed);
            videoIds = body.items[0].id.videoId;
        }

    });

}

function getStore() {
    request('https://fortnite-public-api.theapinetwork.com/prod09/store/get?language=en', {json: true}, async function (err, res, body) {
        if (err) {
            return console.log("Fortnite side");
        }
        if (isEmpty(body.items)) {
            return console.log("Body empty side");
        }
        if (date !== body.date) {
            console.log("Ah changement de la boutique, notif");
            name = [];
            img = [];
            type = [];
            rarity = [];

            for (let i = 0; i < body.items.length; i++) {
                if (body.items[i].featured === 1) {
                    name.push(body.items[i].name + " [En Vedette]");
                } else {
                    name.push(body.items[i].name);
                }
                img.push(body.items[i].item.images.information + "?cache={lastupdate)");
                type.push(body.items[i].item.type);
                rarity.push(body.items[i].item.rarity);

            }
            date = body.date;
        }
    });
}

function getChall() {
    request('https://fortnite-public-api.theapinetwork.com/prod09/challenges/get?season=current', {json: true}, async function (err, res, body) {
        if (err) {
            return console.log("Fortnite side");
        }
        if (isEmpty(body)) {
            return console.log("Body empty side");
        }
        if (season !== body.season) {
            console.log("Ah changement de les chall, notif");
            week = body.currentweek;
            season = body.season;
            stars = [];
            difficulty = [];
            objective = [];

            for (let i = 0; i < 7; i++) {
                stars.push(jp.query(body, "$.challenges.week" + week + "[" + i + "].stars"));
                difficulty.push(jp.query(body, "$.challenges.week" + week + "[" + i + "].difficulty"));
                objective.push(jp.query(body, "$.challenges.week" + week + "[" + i + "].challenge"));
            }

        }
    });

}


bot.login(config.token);
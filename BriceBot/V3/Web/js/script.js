let loop;

function isEven(num) {
    return num % 2 === 0;
}

function change() {

    let socket = io.connect();
    socket.on('store', function (name, img, type, rarity) {


        for (let i = 0; i < 12; i++) {
            if (typeof name[i] === "undefined") {
                document.getElementById("img" + (i + 1)).src = "";
                document.getElementById("name" + (i + 1)).innerHTML = "";
                document.getElementById("type" + (i + 1)).innerHTML = "";
                document.getElementById("rarity" + (i + 1)).innerHTML = "";

            } else {
                document.getElementById("name" + (i + 1)).innerHTML = name[i];
                document.getElementById("img" + (i + 1)).src = img[i] + "?cache={lastupdate)";
                document.getElementById("type" + (i + 1)).innerHTML = "Type d'objet : " + french(type[i]).bold();
                document.getElementById("rarity" + (i + 1)).innerHTML = "Rareté : " + french(rarity[i]).bold();
            }
        }


    })

}

function french(value) {
    if (value === "pickaxe") {
        return "Pioche";
    } else if (value === "emote") {
        return "Emoticône";
    } else if (value === "outfit") {
        return "Skin";
    } else if (value === "rare") {
        return "Rare";
    } else if (value === "epic") {
        return "Epic";
    } else if (value === "uncommon") {
        return "Ordinaire";
    } else if (value === "legendary") {
        return "Légendaire";
    } else if (value === "glider") {
        return "Planeur";
    } else if (value === "hard") {
        return "Difficile";
    } else if (value === "normal") {
        return "Normale";
    } else if (value === "pet") {
        return "Animal";
    } else if (value === "bundle") {
        return "Pack";
    } else if (value === "toy") {
        return "Jouet";
    } else if (value === "backpack") {
        return "Jouet";
    } else {
        return "Aucun objet aujourd'hui";
    }
}

function challenges() {
    let socket = io.connect();
    socket.on('challenges', function (week, objective, stars, difficulty, season) {
        document.getElementById("chal").innerHTML = "Nous sommes la semaine " + String(week).bold() + " de la saison " + String(season).bold() + " de Fortnite";

        for (let i = 0; i <= stars.length; i++) {
            document.getElementById("stars" + (i + 1)).innerHTML = "Étoiles : " + String(stars[i]).bold();
            document.getElementById("difficulty" + (i + 1)).innerHTML = "Difficulté : " + french(String(difficulty[i])).bold();
            document.getElementById("name" + (i + 1)).innerHTML = objective[i];
        }
    });

}


# BriceBot

<div align="center">
  <br />
  <p>
   <img src="https://img.shields.io/badge/DiscordJS%20Version-V11.4.2-brightgreen.svg" alt="DiscordVersion" />
   <img src="https://img.shields.io/badge/Build-passing-success.svg" alt="BuildSuccess" />
   <img src="https://img.shields.io/badge/NodeJS%20Version-V10.14.2-brightgreen.svg" alt="NodeJS" />
  </p>

</div>

BriceBot est l'assistant personnel de Brice, voici ses fonctionnalitées :
  - Notification lors d'une activitée sur la chaine Youtube ou Twitch de Brice.
  - Suppression des messages indésirable dans les salons qui ne sont pas fait pour.
  - Création d'un grade super cool et unique pour recevoir une alerte quand Brice sort une vidéo ou est en live.
  - Le bot héberge un site Web qui affiche la boutique Fortnite du jour.
  - Le bot souhaite la bienvenu aux nouveaux membres.


### Installation

Vous avez besoin de NodeJS ainsi que le framework DiscordJs. <br>
Vous trouverez les liens en bas du ReadMe
Installer les packages mentionner dans le code (Require)

Pour le faire fonctionner il vous faut changer le fichier de config qui se trouve dans le dossier. <br>
Vous avez besoin d'un compte développeur Youtube, clef API Twitch et FortniteAPI et du token du bot Discord.
Faut pas oublier aussi de changer dans le code le nom de la config

Une fois cela changé et DiscordJs installé, vous pouvez lancer le bot via Node.


### Boutique & Futur objets Fortnite 

#### Service WEB
Lors de l'éxecutions du BriceBot, un serveur Web se lance via express.
La page web qui en ressort est un boostrap qui contient le contenu de la boutique Fornite du jour, cette page est automatiquement mise à jour via le bot et l'api Fortnite.
J'ai mit un certificat SSL via Certbot et Let's Encrypt.

#### Service Intra-Discord
Quand vous envoyez la commande `!Boutique` ou `!Coming` dans un des salons du serveur, BriceBot vous enverra dans les prochaines secondes une image avec soit la boutique du jour ou les objets qui arriveront dans Fortnite.

### Socket.io

Pour faire fonctionner les fonctions web vous avez besoin d'installer via npm socket.io en dernière version

### Todos

 - Liaison Twitch pour les subs et NightBot
 - <del>Ajout de l'intra Discord</del>
 - <del>Avertissement des lives sur Twitch</del>

### WatchDog

J'ai aussi créer un autre bot qui permet de m'avertir si le bot principale est plus en ligne, je sais c'est un peu overkill, mais c'est fun!

License
----

MIT

### Sources

NodeJS : https://nodejs.org/en/
DiscordJS : https://discord.js.org/

